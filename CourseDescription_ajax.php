<?php

define('AJAX_SCRIPT', true);
require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once($CFG->libdir .'/filelib.php');
global $CFG,$DB,$USER;
header('Content-Type: application/json');


if ( !function_exists('htmlspecialchars_decode') ) {
    function htmlspecialchars_decode($text)
    {
        return strtr($text, array_flip(get_html_translation_table(HTML_SPECIALCHARS)));
    }
}


require_login();
//check for site Admin
if (!is_siteadmin($USER->id)):
    //not an Admin
    //die('not admin ! '. $USER->id);
    exit;
endif;

//check and set course id
if (empty($_REQUEST['course_id']) || !ctype_digit($_REQUEST['course_id'])):
    //die('no couresID');
    exit;
else:
    $courseId = $_REQUEST['course_id'];
endif;


if (empty($_REQUEST['NewSummary']) || strlen($_REQUEST['NewSummary']) == 0):
    //print_r($_REQUEST);
    //die('no NewSummary :' .  strlen($_REQUEST['NewSummary']) );
    exit;
else:
    $summary = json_decode(stripslashes(htmlspecialchars_decode(urldecode($_REQUEST['NewSummary']))));
//die(print_r($summary));
endif;

$table = 'course_sections';

try {
    $course = $DB->update_record('course',['id' => (int) $courseId, 'summary'=> $summary->datanobtn ] ,false);
    $courseSection = $DB->get_record('course_sections',['course' => (int) $courseId,'section' => 0]);
    $dataObject = ['id' => (int) $courseSection->id, 'summary'=> $summary->data ];
    $courseSectionUpdate = $DB->update_record('course_sections',$dataObject ,false);
    $dataObject = ['id' => (int) $courseSection->id];
    $field = $DB->get_record($table,$dataObject);
    rebuild_course_cache($courseId);
    if($field->summary == $summary->data) {
        echo json_encode(['text' => 'success']);
    }else{
        echo json_encode(['error' => 'not matching']);
    }
} catch (Exception $e) {
    die($e);
    exit;
}

if (!$field):
    echo 'Course Incorrect';
    exit;
endif;

///echo json_encode(['text' => $field->summary]);