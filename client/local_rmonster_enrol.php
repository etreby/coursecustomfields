<?php
// This client for local_wstemplate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * XMLRPC client for Moodle 2 - local_wstemplate
 *
 * This script does not depend of any Moodle code,
 * and it can be called from a browser.
 *
 * @authorr Jerome Mouneyrac
 */

/// SETUP - NEED TO BE CHANGED
$token = '51f45c648423cd61a9df0f339d3f43dd';
//$domainname = 'http://192.155.231.21/moodle';
$domainname = 'https://gateway.mylearnerportal.com/moodle';
$domainname = 'https://training.mylearnerportal.com/dev-clms';

/// FUNCTION NAME
$functionname = 'local_coursecustomfields_enrol';

echo $functionname. "\n";

/// PARAMETERS
$params = '';

///// XML-RPC CALL
header('Content-Type: text/plain');
$server_url = $domainname . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $functionname;
echo $server_url . "\n";

header('Content-Type: text/plain');

require_once('./curl.php');
$curl = new curl;

try {
	$rest_format = ($rest_format == 'json') ? '&moodlewsrestformat=' . $rest_format : '';
	$resp = $curl->post($server_url . $rest_format, $params);	
	echo $resp;
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
