<?php

ini_set('display_errors', 1);

define('NO_MOODLE_COOKIES', true);
define('AJAX_SCRIPT', true);
require_once(dirname(dirname(__DIR__)) . '/config.php');

if (empty($_GET['fieldId']) || !ctype_digit($_GET['fieldId']))
    exit;

global $DB;

try {
    $field = $DB->get_record('customfield', ['id' => (int) $_GET['fieldId']]);
}
catch (Exception $e) {
    exit;
}

if (!$field)
    exit;

header('Content-Type: application/json');
echo json_encode(['heading' => 'Description', 'text' => $field->description]);