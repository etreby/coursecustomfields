<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//85645
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
   	/*  	
    a list => external_multiple_structure
    an object => external_single_structure
    a primary type => external_value
    */

require_once($CFG->dirroot."/cohort/lib.php");		
require_once($CFG->libdir."/externallib.php");


function coursecustomfields_cron_run() {
	global $CFG;
	local_coursecustomfields::enrol();
}

class local_coursecustomfields extends external_api {

	function get_bool($value){
		switch( strtolower($value) ){
			case 'true': return true;
			case 'false': return false;
			default: return NULL;
		}
	}
	
    /********************************************************/    
    /**
     * Get role id
     */
    /********************************************************/       
    public static function get_role_id($rolename) {
    	global $DB;
    
    	$role= $DB->get_record('role', array('shortname' => $rolename), '*', MUST_EXIST);
    	if($role == null)
    		return -1;
    	else
    		return $role->id;
    }      
    /********************************************************/
    
    /********************************************************/
    public static function enrol_parameters() {
       return new external_function_parameters(array());       
    }
    
    public static function msg_out($msg){
    	global $CFG;
   	
    	$file = $CFG->dirroot.'/local/coursecustomfields/logs/msglog.txt';
    	$line =	date(DATE_ATOM).'  '.$msg;
    	file_put_contents($file, $line.PHP_EOL , FILE_APPEND);    	
    }
    
    public static function test() {    	    	
    	global $DB;
    	local_coursecustomfields::msg_out("in test()");
    	
    	//$cohort = $DB->get_record('cohort', array('idnumber' => 'cohort1'), '*', MUST_EXIST);
    	//var_dump($cohort);
    	
    	//$cohortids = $DB->get_records('user', null, null, 'id');
    	//$cohorts = array();
    	//foreach($cohortids as $id) {
    	//	$cohort = $DB->get_record('user', array('id' => $id->id), '*', MUST_EXIST);
    	//	var_dump($cohort);
    	//}   
    	 	 
    	
    	$cohort = $DB->get_record('cohort', array('idnumber' => 'cohort1'), '*', MUST_EXIST);    	
    	if (!$DB->record_exists('cohort_members', array('cohortid'=>$cohort->id, 'userid'=>27))) {
    		local_coursecustomfields::msg_out("abt to cohort_add_member()");
    		
    		cohort_add_member($cohort->id, 27);
    		
    		// Get the cohort classes
    		
    			// Enroll in them
 /*   		
    		$record = new stdClass();
    		$record->cohortid  = $cohortid;
    		$record->userid    = $userid;
    		$record->timeadded = time();
    		$DB->insert_record('cohort_members', $record);
    		
    		$cohort = $DB->get_record('cohort', array('id' => $cohortid), '*', MUST_EXIST);
    		
    		$event = \core\event\cohort_member_added::create(array(
    				'context' => context::instance_by_id($cohort->contextid),
    				'objectid' => $cohortid,
    				'relateduserid' => $userid,
    		));
*/    		    		
    	} 
    	
    	local_coursecustomfields::msg_out("done with cohort_add_member()");    	
    }
    
    public static function enrol() {   
     	global $CFG;
     	global $DB;
     	     	
     	$rc = 0;    	 	       	    	
    	local_coursecustomfields::msg_out("Starting...");
    	
    	$settings = $CFG->dirroot.'/local/coursecustomfields/settings.xml';
    	 	    	 
    	try {
    		$newTime = time();    		    	
    		$xml = simplexml_load_file($settings);
    		    		      	
    		// Load the last time ran		
    		$timestamp = file_get_contents($CFG->dirroot.'/local/coursecustomfields/timestamp.txt');
    		
 	    	//local_coursecustomfields::msg_out('Timestamp: '.$timestamp);
	    		    	
	    	$studentRoleId = local_coursecustomfields::get_role_id("Student");	    	   	    	
	    	
	    	// Search Moodle database for all site badges awarded last X minutes	    			    
	    	$sql = 'SELECT * FROM {badge} b JOIN {badge_issued} bi ON b.id = bi.badgeid where bi.dateissued >= '. $timestamp;
	    	$badgesAwarded = $DB->get_records_sql($sql);
	    	
	    	$sql = 'SELECT * FROM {badge} b JOIN {badge_issued} bi ON b.id = bi.badgeid where bi.dateexpire >= '. $timestamp;
	    	$badgesExpired = $DB->get_records_sql($sql);
	    	
	    	// Add this run to the log file
	    	local_coursecustomfields::log_run();
	    	
   			if(count($badgesAwarded)==0 && count($badgesExpired)==0) {
		    	// Save the current time	    
   				local_coursecustomfields::msg_out('No badges awarded or expired after: '.date('Y-m-d H:i:s', $timestamp) );  				
    			local_coursecustomfields::msg_out('Ending...');
   				return 0;
   			}
   			
   			local_coursecustomfields::msg_out('Badges Awarded: '.count($badgesAwarded).' Badges expired: '. count($badgesExpired));
   			
	    	// For all the tracks
	    	$x=0;	    	
	    	// Good  
	    	foreach ($xml->tracks->track as $track) {	    		
		    	local_coursecustomfields::msg_out("Track is ".$track->name);
	    		
	    		$emailTemplate = "";	
	    		foreach ($track->badges->badge as $badge) {	    				
    				$badge_name = $badge->name;
    				local_coursecustomfields::msg_out("  Badge: ".$badge_name.' condition: '.$badge->enrol_when);

    				if(strcmp($badge->enrol_when,'is awarded')==0)
    					$badge_list = $badgesAwarded;
    				else 
    					$badge_list = $badgesExpired;
    				
    				foreach($badge_list as $award) {    				
	    				local_coursecustomfields::msg_out($badge->enrol_when." ".$award->name.", track badge ".$badge_name);
	    				 
		    			// If this badge starts with one in the track		    			
		    			if($award->name == $badge_name) {		    				
		    				// Get the user
		    				$user = get_complete_user_data('id', $award->userid);		    				
		    				
		    				// Enroll in the next badge
		    				$rc = local_coursecustomfields::enroll_user_in_units($user, $badge, $studentRoleId, $track);
		    				if($rc==0)     							    				
			    				$status = "success";
		    				else 
		    					$status = "failed";
		    			}
    				}
	    		}			    		    		    	
	    	}
	    	
	    	// Save the current time if no error	
	    	if($rc==0)    
	    		file_put_contents($CFG->dirroot.'/local/coursecustomfields/timestamp.txt', $newTime);
    	}
    	catch (exception $e) {
    		local_coursecustomfields::msg_out("Exception>>". $e);
    	} 
    	 	   	
    	local_coursecustomfields::msg_out("Ending...");
    	
    	return $rc;
    }
    
    public static function enrol_returns() {
    	return new external_value(PARAM_INT, 'return code');    	 
    }
    /********************************************************/    
    
    /**
     * Enroll a urse in a list of units (cohort, badge, or course)
     * 
     * @param unknown $userid
     * @param unknown $next_units
     * @param unknown $roleId
     */
    public static function enroll_user_in_units($user, $badge, $roleId, $track) {
    	local_coursecustomfields::msg_out("   enroll_user_in_units(): userid ".$userid." email_enabled: ".$email_enabled);  
    	
    	$rc = 0;
    	//var_dump($user);
    	foreach ($badge->next_units->children() as $unit) {    		
    		local_coursecustomfields::msg_out("      ".$unit->type);  

    		if($unit->type == "enrol in course") {
    			$rc = local_coursecustomfields::enroll_user_in_course($user->id, (string) $unit->name, $roleId);
    		}
    		else if($unit->type == "enrol in badge") {
    			$rc = local_coursecustomfields::enroll_user_in_badge($user->id, (string) $unit->name, $roleId);
    		}
    	    else if($unit->type == "enrol in cohort") {
    	    	$rc = local_coursecustomfields::enroll_user_in_cohort($user->id, (string) $unit->name);    	    	
    		} 
    		//else log this    		
    		
    		if(rc==0) {
    			$line =	date(DATE_ATOM).",".$user->username.",".$unit->type.",".(string) $unit->name.",success";
    			
    			local_coursecustomfields::log_enrolment($line);
    			
    			// Email the user
    			if(get_bool($track->email_enabled)) {
    				local_coursecustomfields::email_user($user, (string) $track->email_body, $unit, $track);
    			}    			 
    		}
    		else {
    			$line =	date(DATE_ATOM).",".$user->username.",".$unit->type.",".(string) $unit->name.",failed";
    			return rc;
    		}    		
      	}
    }

    /**
     * Status: function complete
     * TODO: Harden, add event tracking
     * @param unknown $userid
     * @param unknown $cohort_name
     * @param unknown $roleId
     * @return number
     */
    public static function enroll_user_in_cohort($userid, $cohort_name) {
    	global $DB;
    	local_coursecustomfields::msg_out("         enroll_user_in_cohort(): userid:".$userid." cohort:".$cohort_name);
    
    	$rc = 0;
    	try {
    		$cohort = $DB->get_record('cohort', array('name' => $cohort_name), '*', MUST_EXIST);
    		cohort_add_member($cohort->id, $userid);
    		
    		$event = \core\event\cohort_member_added::create(array(
			    				'context' => context::instance_by_id($cohort->contextid),
			    				'objectid' => $cohort->id,
			    				'relateduserid' => $userid,
			    				'component' => 'Badge Enroler',
			    		));    		
    	}
    	catch (dml_missing_record_exception $e) {
    		local_coursecustomfields::msg_out('Cohort '.$cohort_name.' not found');
    		$rc = -1;
    	}
    	catch (exception $e) {
    		local_coursecustomfields::msg_out("Exception>>". $e);
    		$rc = -1;
    	}
    
    	return $rc;
    }
    
    /**
     * Status: function complete
     * TODO: Harden, add event tracking
     * @param unknown $userid
     * @param unknown $badge_name
     * @param unknown $roleId
     * @throws moodle_exception
     * @return number
     */
    public static function enroll_user_in_badge($userid, $badge_name, $roleId) {

    	global $DB;
    	local_coursecustomfields::msg_out("         enroll_user_in_badge: ".$userid.",".$badge_name);
    	$rc = 0;
    	try {
    		// Retrieve the manual enrolment plugin.
    		$enrol = enrol_get_plugin('manual');
    		if (empty($enrol)) {
    			throw new moodle_exception('manualpluginnotinstalled', 'enrol_manual');
    		}
    			
    		$courseids = local_coursecustomfields::get_courses_in_badge_ex($badge_name);
    		foreach($courseids as $courseid) {
    			$instances = enrol_get_instances($courseid, true);
    
    			if (empty($instances)) {
    				throw new moodle_exception('nocourseinstance', 'enroll_user_in_badge');
    			}
    			 
    			if(count($instances)==1) {
    				foreach($instances as $instance) {
    					$enrol->enrol_user($instance, $userid, $roleId);
    				}
    			}
    			else if(count($instances)==0 || count($instances)>1)
    				$rc = -1;
    		}
    	}
        catch (dml_missing_record_exception $e) {
    		local_coursecustomfields::msg_out('Badge '.$badge_name.' not found');
    	}
    	catch (exception $e) {
    		local_coursecustomfields::msg_out("Exception>>". $e);
    		$rc = -1;
    	}
    
    	return $rc;
    }
        
    /**
     * Email the end user. 
     * Read the email form the template, substitue some fields and send
     * @param unknown $user
     * @param unknown $emailTemplate
     * @param unknown $badge_name
     * @param unknown $track
     */
    public static function email_user($user, $emailTemplate, $unit, $track) {
    	local_coursecustomfields::msg_out("         email_user: ".$user->email.",".(string) $unit->name.",".$track->email_subject.','.$emailTemplate);
    	  
    	$email_body = local_coursecustomfields::prepEmail($user, $unit, $emailTemplate);
    	//local_coursecustomfields::msg_out("         email_body: ".$email_body);
    	 
    	// To send HTML mail, the Content-type header must be set
    	$headers  = 'MIME-Version: 1.0' . "\r\n";
    	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    	
    	// Additional headers
     	$headers .= 'From: '.(string) $track->email_from . "\r\n";
   	    	
    	$email_address = $user->email;
    	
    	//local_coursecustomfields::msg_out("in email_user() ".$email."\n";
    	mail ($email_address , $track->email_subject, $email_body, $headers);  	     	  
    }
    
    public static function prepEmail($user, $unit, $emailTemplate) {
    	global $CFG;
    	$emailfile = $CFG->dirroot.'/local/coursecustomfields/'.$emailTemplate;     	 
    	$email = file_get_contents($emailfile);    	 
    	$email = str_replace('$firstname', $user->firstname, $email);
    	$email = str_replace('$lastname', $user->lastname, $email);    	 
    	$email = str_replace('$unitname', (string) $unit->name, $email);    	
    	
    	if(strcmp($unit->type, 'enrol in course')==0)
    		$email = str_replace('$unittype', 'course', $email);
    	else if(strcmp($unit->type, 'enrol in cohort')==0)
    		$email = str_replace('$unittype', 'cohort', $email);
    	else if(strcmp($unit->type, 'enrol in badge')==0)
    		$email = str_replace('$unittype', 'badge', $email);
    	 
    	return $email;
    }
    
    public static function log_enrolment($file, $line) {  
    	if($file != null) {  
    		$myfile = file_put_contents($file, $line.PHP_EOL, FILE_APPEND);
    	}
    }
    
    public static function log_run() {
    	global $CFG;    	 
    	$file = $CFG->dirroot.'/local/coursecustomfields/logs/runlog.txt';    	 
   		$line =	'Ran ' . date(DATE_ATOM);
   		file_put_contents($file, $line.PHP_EOL , FILE_APPEND);
    }
    
    public static function enroll_user_in_course($userid, $course_name, $roleId) {
    	local_coursecustomfields::msg_out("      enroll_user_in_course(): userid:".$userid." course:".$course_name." roleid:".$roleId);    	
    	   
    	$rc = 0;
    	global $DB;
    	try {   		 
    		$enrol = enrol_get_plugin('manual');
    		if (empty($enrol)) 
	    		throw new moodle_exception('manualpluginnotinstalled', 'enroll_user_in_course');
    		
    		// Get the course id    	
    		$course = $DB->get_record('course', array('fullname' => $course_name), '*', MUST_EXIST);    	
    		if (count($course)==0) {
        		local_coursecustomfields::msg_out('         Course '.$course_name.' not found');
        		return -1;
    		}
    		
    		// Enroll in the first instance
   			$instances = enrol_get_instances($course->id, true);	     			
   			if (empty($instances)) {
  				throw new moodle_exception('coursehasnoinstances', 'enroll_user_in_course');
  			}
  			
//  			foreach($instances as $instance) {
// 				var_dump($instances);
//  			}
  					
  			if(count($instances)!=0) {
  				foreach($instances as $instance) {
        			//local_coursecustomfields::msg_out("         ".$instance->enrol);
  					if(strcmp($instance->enrol,'manual')==0) {
        				local_coursecustomfields::msg_out("         found manual enrol");
  						$enrol->enrol_user($instance, $userid, $roleId);
  					}
  				}
  			}
  			else if(count($instances)==0 || count($instances)>1) {
  				$rc = -1;	    				    		
	      		local_coursecustomfields::msg_out('        '.count($instances).' instances of course');
  			}
    	}
        catch (dml_missing_record_exception $e) {
        	local_coursecustomfields::msg_out("         enroll_user_in_course: Record not found: ".$e->params[0]);
    	}
    	catch (exception $e) {
     		local_coursecustomfields::msg_out("Exception>>". $e);
	    	$rc = -1;			
     	}
     	
     	if(rc==0)
     		local_coursecustomfields::msg_out("         enrolled user ".$userid." in course ".$course_name);
		else     	
			local_coursecustomfields::msg_out("         error enrolling user ".$userid." in course ".$course_name);

     	return $rc;
    }
}
