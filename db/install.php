<?php

function xmldb_local_coursecustomfields_install() {
	/** @var mariadb_native_moodle_database $DB */
	global $DB;

	$prefix = $DB->get_prefix();
	
	$DB->execute("
		CREATE TABLE `{$prefix}customfield` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(128) NOT NULL COLLATE 'utf8_unicode_ci',
			`type` TINYINT(4) NOT NULL,
			`default_value` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
			`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
			`required` TINYINT(4) NOT NULL,
			`domain` TINYINT(4) NOT NULL,
			`active` TINYINT(4) NOT NULL,
			`createdate` DATETIME NOT NULL,
			`field_order` INT(11) NULL DEFAULT NULL,
			PRIMARY KEY (`id`)
		)
		COMMENT='Stores customfield names for course, offering etc'
		COLLATE='utf8_unicode_ci'
		ENGINE=InnoDB
	");

	$DB->execute("
		CREATE TABLE `{$prefix}customfield_option` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`customfield_id` INT(11) NOT NULL,
			`val` VARCHAR(256) NOT NULL,
			`display_order` INT(11) NOT NULL,
			PRIMARY KEY (`id`),
			INDEX `FK__clms_customfield` (`customfield_id`),
			CONSTRAINT `FK__clms_customfield` FOREIGN KEY (`customfield_id`) REFERENCES `{$prefix}customfield` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
		)
		COMMENT='Stores option values of course etc. custom attributes for drop down menus etc.\r\n'
		COLLATE='latin1_swedish_ci'
		ENGINE=InnoDB
	");

	$DB->execute("
		CREATE TABLE `{$prefix}customfield_value` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`customfield_id` INT(11) NOT NULL,
			`ref_id` INT(11) NOT NULL,
			`value` VARCHAR(256) NOT NULL COLLATE 'utf8_unicode_ci',
			`createdate` DATETIME NOT NULL,
			PRIMARY KEY (`id`),
			INDEX `FK_clms_customfield_value_clms_customfield` (`customfield_id`),
			CONSTRAINT `FK_clms_customfield_value_clms_customfield` FOREIGN KEY (`customfield_id`) REFERENCES `{$prefix}customfield` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
		)
		COMMENT='Stores custom field values for referenced item'
		COLLATE='utf8_unicode_ci'
		ENGINE=InnoDB 
	");
}