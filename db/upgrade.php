<?php

function xmldb_local_coursecustomfields_upgrade($oldversion)
{
    /** @var mariadb_native_moodle_database $DB */
	global $DB;

	if ($oldversion < 2016070811) {
		$prefix = $DB->get_prefix();

		return $DB->execute("
			ALTER TABLE 
			  `{$prefix}customfield`
			ADD COLUMN 
			  `description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci'
			AFTER
			  `default_value`
		");
	}

	return true;
}