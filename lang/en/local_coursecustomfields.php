<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'Course Custom Fields';

// Buttons
$string['refresh'] = 'Refresh';
$string['define'] = 'Define Fields';
$string['reset'] = 'Reset';
$string['set'] = 'Set Course Fields';
$string['add'] = 'Add';
$string['getcourse'] = 'Get Course Fields';
$string['showaddfldform']  = 'Add a Field';
$string['cancel'] = 'Finished';
$string['update'] = 'Update';

// Messages
$string['msg_namerequired'] = 'Please specify a "Name".';
$string['msg_dfrequired'] = 'Please specify a "Default Value".';
$string['msg_dd_values_required'] = 'Please specify the dropdown list values.';
$string['msg_requiredrequired'] = 'Please select a "Required" option.';
$string['msg_active_required'] = 'Please select an "Active" option.';
$string['msg_field_required'] = 'This field is required.';
$string['msg_required_fields'] = 'One or more required fields are missing!';
$string['msg_order_number'] = 'The order field must be a number.';
$string['msg_type_required'] = 'The field type is required! Please select the correct field type from the dropdown!';

// Headers
$string['define_title'] = 'Defined Fields';
$string['course_name'] = 'Course Name';
$string['add_field'] = 'Add Custom Field';
$string['edit_field'] = 'Edit Custom Field';

// Input types
$string['input_type'] = 'Text';
$string['dropdown_type'] = 'Dropdown';
$string['checkbox_type'] = 'Checkbox';
$string['unknown_type'] = 'Unknown';

// Field titles
$string['field_name'] = 'Name';
$string['field_default_value'] = 'Default Value';
$string['field_type'] = 'Input Type';
$string['field_dd_values'] = 'Dropdown list values';
$string['field_required'] = 'Require (Y/N)';
$string['field_domain'] = 'Domain';
$string['field_active'] = 'Active (Y/N)';
$string['field_order'] = 'Order';
$string['field_cmds'] = 'Commands';
$string['field_description'] = 'Description';

// Hover
$string['hdel'] = 'Delete';
$string['hedt'] = 'Edit';

// Description texts
$string['description_title'] = 'Description';

$string['show_only_existing'] = 'Show only courses with added fields';

//Widget Creator
$string['generate'] = 'Generate';
$string['save_widget'] = 'SaveWidget';
$string['please_wait'] = 'Please Wait..<blink>.</blink>';
$string['saved'] = 'Saved';
$string['failed'] = 'Failed !';

//$string[''] = '';
//$string[''] = '';


