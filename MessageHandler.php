<?php

namespace local\coursecustomfields;

/**
 * Handling information, success and error messages in the course custom fields plugin
 */
class MessageHandler
{
    /**
     * @var array
     */
    protected static $informational = [];

    /**
     * @var array
     */
    protected static $success       = [];

    /**
     * @var array
     */
    protected static $error         = [];

    /**
     * @var array
     */
    protected static $errorData     = [];

    /**
     * Adds an informational message
     *
     * @param string $message
     */
    public static function addInformationalMessage($message)
    {
        self::$informational[] = $message;
    }

    /**
     * Adds an successful message
     *
     * @param string $message
     */
    public static function addSuccessMessage($message)
    {
        self::$success[] = $message;
    }

    /**
     * Adds an error message
     *
     * @param string $message
     * @param null   $data
     */
    public static function addErrorMessage($message, $data = null)
    {
        self::$error[] = $message;

        if (!empty($data)) {
            self::$errorData[] = $data;
        }
    }

    /**
     * Are there any error messages added
     *
     * @return bool
     */
    public static function hasErrors()
    {
        return !empty(self::$error);
    }

    /**
     * Get error message from the class
     *
     * @return array
     */
    public static function getErrors()
    {
        return self::$error;
    }

    /**
     * Get data associated with the errors
     *
     * @return array
     */
    public static function getErrorData()
    {
        return self::$errorData;
    }

    /**
     * Has success messages added ?
     *
     * @return bool
     */
    public static function hasSuccess()
    {
        return !empty(self::$success);
    }

    /**
     * Return success messages
     *
     * @return array
     */
    public static function getSuccess()
    {
        return self::$success;
    }
}