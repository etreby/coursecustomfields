<?php

use local\coursecustomfields\MessageHandler;

class local_coursecustomfields_renderer extends plugin_renderer_base
{
	const TEXT_INPUT = 1;
	const DROPDOWN_INPUT = 2;
	//const CHECKBOX_INPUT = 3;	
		
	public $behome = '';
	private $yesno = array(1=>'No', 2=>'Yes');

    /**
     * Fields name map
     *
     * @var array
     */
    protected $fieldNames = [
        'name'          => ['form' => 'field_name'          , 'db' => 'name'],
        'type'          => ['form' => 'field_type'          , 'db' => 'type'],
        'default_value' => ['form' => 'field_default_value' , 'db' => 'default_value'],
        'dd_values'     => ['form' => 'field_dd_values'     , 'db' => ''],
        'description'   => ['form' => 'field_description'   , 'db' => 'description'],
        'required'      => ['form' => 'field_required'      , 'db' => 'required'],
        'active'        => ['form' => 'field_active'        , 'db' => 'active'],
        'order'         => ['form' => 'field_order'         , 'db' => 'field_order'],
    ];

    /**
     * Show the initial page with 2 buttons - one for defining field and one for setting them
     */
	public function show()
	{
		echo '
			<form action="index.php" method="post">
				<input type="submit" name="act_define" id="define" value="' . get_string('define', 'local_coursecustomfields') . '">
				<input type="submit" name="act_show_courses_page" id="set" value="' . get_string('set', 'local_coursecustomfields') . '" />
			</form>
		';
	}

    /**
     * Custom course fields filling page.
     *
     * @param array $formData
     */
	public function showEnterCourseValuesPage($formData)
    {
        global $DB, $PAGE;

        $PAGE->requires->js('/local/coursecustomfields/javascript/utils.js');
        $useWidget = true;



        $courseId = empty($formData['course_id']) ? NULL : $formData['course_id'];
        $prev_course_id = empty($formData['prev_course_id']) ? "" : $formData['prev_course_id'];       
        $show_only_existing = empty($formData['act_show_only_existing']) ? "" : " checked ";       
        //$show_only_existing = " checked ";
        //var_dump($formData['ccf_show_only_existing']);
        $this->showMessages();

        $form =
            '<form action="index.php" method="post" id="course-custom-fields-fill-form">'
                . html_writer::label(get_string('course_name', 'local_coursecustomfields') . ':', 'inp-course-id')
                . $this->courseSelectDropDown($courseId)
                . '<input type="submit" name="act_show_courses_page" value="' . get_string('getcourse', 'local_coursecustomfields') . '">'
                . '<br>'
                . '<input type="checkbox" name="act_show_only_existing" id="ccf-filter-courses-checkbox" '.$show_only_existing.'> ' . get_string('show_only_existing', 'local_coursecustomfields')
                . '<br><br>';
        //etreby added 20170806
        if($useWidget && $courseId != NULL):
            $WidgetFieldName = array('SKILL_LEVEL','DURATION','DELIVERY_TYPE_DETAILED','SHORT_SUMMARY','SOURCE_URL','IMAGE_URL');
            $WidgetFieldsKeys = array();
            $form .= '<div class="ibm-columns">'
                       .  '<div class="ibm-col-6-3" style="width: 50%;">';
        endif;

        // If there is a course selected - show fields
        if (!empty($courseId)) {
            $customFields   = $DB->get_records('customfield', NULL, 'field_order');
            $existingValues = $DB->get_records_menu('customfield_value', ['ref_id' => $courseId], '', 'customfield_id, value');
            $required       = '<span style="color:#F00;">*</span>';

            foreach ($customFields as $customField) {
                if ($customField->active != DB_YES)
                    continue;

                $fieldName = 'field_' . $customField->id;
                //etreby added 20170809
                if($useWidget && in_array($customField->name,$WidgetFieldName)):
                        $WidgetFieldsKeys[trim($customField->name)] = $customField->id;
                endif;

                // If the course is the same as last time
                if(strcmp($courseId,$prev_course_id)==0) {
                	//echo 'Same';
                	//$msg =$customField->name.": ";
                	if (!$value = (empty($formData[$fieldName]) ? '' : $formData[$fieldName])) {
                		//$msg = $msg . 'no form data';
	               		if (!$value = (empty($customField->default_value) ? '' : $customField->default_value)) {
	               			//$msg = $msg . ', no default data';	               			 
    	            		if (!$value = (empty($existingValues[$customField->id]) ? '' : $existingValues[$customField->id])) {
    	            			//$msg = $msg . ', no existing data';    	            			
    	            			$value = '';                	 
    	            		}
	               		}
                	}
                	//echo $msg.'<br>';
                }
                else { // Different course
                	//echo 'Different';
                	if (!$value = (empty($customField->default_value) ? '' : $customField->default_value))
                		if (!$value = (empty($existingValues[$customField->id]) ? '' : $existingValues[$customField->id]))
                			$value = '';
                }
                

                $descriptionIcon = empty($customField->description) ? '' : $this->renderHelpIcon($customField->id) . ' ';
                $title           = $customField->name . ($customField->required ? $required : '') . ': ' . $descriptionIcon;

                $requiredError = in_array($fieldName, MessageHandler::getErrorData())
                                    ? '<div style="color: #D00;">' . get_string('msg_field_required', 'local_coursecustomfields') . '</div>'
                                    : '';

                switch ($customField->type) {
                    case self::TEXT_INPUT:
                        $form .= html_writer::label($title, 'inp-field-' . $customField->id)
                              .  $requiredError
				              .  html_writer::empty_tag('input', ['type' => 'text', 'name' => $fieldName, 'value' => $value, 'id' => 'inp-field-' . $customField->id]);
                        break;

                    case self::DROPDOWN_INPUT:
                        $dropDownFieldValues = $DB->get_records_menu('customfield_option', ['customfield_id' => $customField->id], 'display_order', 'id, val');

                        $form .= html_writer::label($title, 'inp-field-' . $customField->id)
                              .  $requiredError
                              .  html_writer::select($dropDownFieldValues, $fieldName, $value, ['' => 'choosedots'], ['id' => 'inp-field-' . $customField->id]);
                        break;
                }
            }
        }

        $form .= '<br>'
              .  '<input type="submit" name="act_set_course_fields" value="' . get_string('set', 'local_coursecustomfields') . '">'
              .  '<input type="submit" name="act_cancel" value="' . get_string('cancel', 'local_coursecustomfields') . '">'
              .  '<input type="hidden" name="prev_course_id" id="prev_course_id" value="' . $courseId.'">';
        //etreby added 20170806
        if($useWidget && $courseId != NULL):
            $form .=  '</div>'
                        .'<div class="ibm-col-6-3" style="width: 50%;">'
                            .'<div><textarea id="output" rows="15" cols="45"></textarea></div>'
                            .'<div><textarea id="outputnobtn" style="display: none; visibility: hidden;" rows="15" cols="45"></textarea></div>'
                            .'<div id="preview" style=" background-color: white;box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);padding: 10px;transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1) 0s;"></div>'
                            .'<button id="GenerateWidgetBtn" data-fields="'.str_replace('"',"'",trim(json_encode($WidgetFieldsKeys))).'" style="margin-top: 20px;" type="button" class="ibm-btn-pri ibm-btn-blue-50" onclick="generate();">' . get_string('generate', 'local_coursecustomfields') . '</button>'
                            .'<button id="SaveWidgetBtn" style="margin-top: 20px;" type="button" class="ibm-btn-pri ibm-btn-blue-50" onclick="saveCourseDescription('.$courseId.',' ."'" . get_string('please_wait', 'local_coursecustomfields') ."'". ',' ."'" . get_string('saved', 'local_coursecustomfields') ."'". ',' ."'" . get_string('failed', 'local_coursecustomfields') ."'". ');">' . get_string('save_widget', 'local_coursecustomfields') . '</button>'
                            .'<div id="SaveWidgetStatus"></div>'
                        .'</div>'
                        .'</div>';
        endif;

        $form .=   '</form>';
        //etreby added 20170809
        if ($useWidget):
            $PAGE->requires->js('/local/coursecustomfields/javascript/widgetMaker.js');
        endif;
        echo $form;
    }

    /**
     * Show any messages if added
     */
    public function showMessages()
    {
        $output = '';

        if (MessageHandler::hasErrors()) {
            $output .= '<div id="local-ccf-error-messages">' . implode('<br>', MessageHandler::getErrors()) . '</div>';
        }

        if (MessageHandler::hasSuccess()) {
            $output .= '<div id="local-ccf-success-messages">' . implode('<br>', MessageHandler::getSuccess()) . '</div>';
        }

        echo $output;
    }

    /**
     * Generate course select drop down (with ability to be filtered)
     *
     * @param string $selected
     *
     * @return string
     */
    public function courseSelectDropDown($selected = '')
    {
        global $DB;

        $all = $DB->get_records_menu('course', null, 'sortorder ASC', 'id, shortname');
        $existing = array_flip($DB->get_fieldset_sql("SELECT DISTINCT ref_id FROM {customfield_value}"));

        $html = '<option value="">' . get_string('choosedots') . '</option>';

        foreach ($all as $key => $value) {
            $html .= '<option value="' . $key . '"' . ($key == $selected ? ' selected' : '') . (isset($existing[$key]) ? '' : ' new') . '>' . $value . '</option>';
        }

        $html = '<select name="course_id" id="inp-course-id">' . $html . '</select><select id="inp-course-id-dummy" style="display:none;"></select>';

        return $html;
    }
	
	/**
	 *  Show the list of course custom fields 
	*/	
	public function show_define_page($home) {
		global $DB;

		echo '<h4>' . get_string('define_title', 'local_coursecustomfields') . '</h4>';

        $this->showMessages();
	
		echo '<form action="index.php" method="post">';
	
		$recs = $DB->get_records('customfield', ['domain' => 1], "field_order ASC");

		$table = new html_table();
        $table->size = ['20%', '30%', '20%', '8%', '8%', '8%', '6%'];
        $table->head = [
            get_string('field_name'         , 'local_coursecustomfields'),
            get_string('field_description'  , 'local_coursecustomfields'),
            get_string('field_default_value', 'local_coursecustomfields'),
            get_string('field_type'         , 'local_coursecustomfields'),
            get_string('field_required'     , 'local_coursecustomfields'),
            get_string('field_active'       , 'local_coursecustomfields'),
            get_string('field_cmds'         , 'local_coursecustomfields'),
        ];
	
		// existing items
		foreach ($recs as $rec) {				
			$hvr = get_string('hedt', 'local_coursecustomfields');
			$command_cell = '<a href="'.$home.'?act_editfld&amp;field='.$rec->id.'"><img title="'.$hvr.'" src="'.$this->pix_url('t/edit').'" alt="'.$hvr.'"></a>';
			
			$hvr = get_string('hdel', 'local_coursecustomfields');
			$command_cell= $command_cell.'   <a href="'.$home.'?act_delfld&amp;field='.$rec->id.'"><img title="'.$hvr.'" src="'.$this->pix_url('t/delete').'" alt="'.$hvr.'"></a>';

            $table->data[] = [
                $rec->name,
                $rec->description,
                $rec->default_value,
                $this->get_type_text($rec->type),
                $this->yesno[$rec->required + 1],
                $this->yesno[$rec->active + 1],
                $command_cell
            ];
		}

		echo html_writer::table($table);
		
		echo '<br><input type="submit" name="act_showaddfldform" id="showaddfldform" value="'
				. get_string('showaddfldform', 'local_coursecustomfields') . '" />';
	
		echo '<input type="submit" name="act_cancel" id="cancel" value="'
				. get_string('cancel', 'local_coursecustomfields') . '" />';
		echo '</form>';
	}

    /**
     * Render form for adding/editing a new custom field
     *
     * @param $mode         - Adding or editing ?
     * @param $form_vars    - User input data
     * @param $rec          - Record form DB
     */
	public function render_add_field_form($mode, $form_vars, $rec)
	{
		global $PAGE;

		$PAGE->requires->js('/local/coursecustomfields/javascript/utils.js');		
		$type_list = [
            1 => get_string('input_type', 'local_coursecustomfields'),
            2 => get_string('dropdown_type', 'local_coursecustomfields')
        ];
		//3=>get_string('checkbox_type', 'local_coursecustomfields'));

        $isNew = $mode === 'add';

        $renderOutput = $isNew
                        ? '<h4>'.get_string('add_field', 'local_coursecustomfields').'</h4>'
                        : '<h4>'.get_string('edit_field', 'local_coursecustomfields').'</h4>';
							
		$field_id = empty($form_vars['field_id'])
                    ? (empty($rec->id) ? null : $rec->id)
                    : $form_vars['field_id'];

        $required = '<span style="color:red">*</span>';

        // Show messages
        $this->showMessages();

        /**
         * Resolve value either form input form (form_vars) or by value in the DB (rec)
         *
         * @param $value
         *
         * @return string
         */
        $resolveValue = function($value) use ($form_vars, $rec) {
            return empty($form_vars[$this->fieldNames[$value]['form']])
                    ? (empty($rec->{$this->fieldNames[$value]['db']}) ? '' : $rec->{$this->fieldNames[$value]['db']})
                    : $form_vars[$this->fieldNames[$value]['form']];
        };

        if(!empty($this->msg))
			echo '<span style="color: red; font-weight: bold;">' . $this->msg->text . '</span>';

        /**
         * Form rendering
         */
            // Name
            $formContent   =  html_writer::label(get_string('field_name', 'local_coursecustomfields') . ': ' . $required, 'inp-field-name')
                            . html_writer::empty_tag('input', ['type'=>'text', 'name'=>'field_name', 'id' => 'inp-field-name', 'value' => $resolveValue('name')]);

            // Default value
            $formContent   .= html_writer::label(get_string('field_default_value', 'local_coursecustomfields') . ': ', 'inp-field-default-value')
                            . html_writer::empty_tag('input', ['type'=>'text', 'name'=>'field_default_value', 'id' => 'inp-field-default-value', 'value' => $resolveValue('default_value')]);

            // Field type
            $formContent   .= html_writer::label(get_string('field_type', 'local_coursecustomfields') . ': ' . $required, 'inp-field-type')
                            . html_writer::select(
                                $type_list,
								'field_type',
								$resolveValue('type'),
                                ['' => 'choosedots'],
                                ['onchange'=>'ccf_field_type_change(this)', 'id' => 'inp-field-type']
                            );

            // Drop-down options (if drop-down type is selected
            $formContent   .= html_writer::start_div('ccf_field_dropdown_values', ['style' => 'display: ' . ($resolveValue('type') == '2' ? 'block' : 'none')])
                            . html_writer::label(get_string('field_dd_values', 'local_coursecustomfields') . ': ' . $required, 'inp-field-drop-down-values')
                            . html_writer::empty_tag('input', ['type'=>'text', 'name'=>'field_dd_values', 'id' => 'inp-field-drop-down-values', 'value' => $this->getDropDownData($field_id)])
                            . html_writer::end_div();
        
            // Description
            $formContent   .= html_writer::label(get_string('field_description', 'local_coursecustomfields') . ': ', 'inp-field-description')
                            . html_writer::tag('textarea', $resolveValue('description'), ['name'=>'field_description', 'id' => 'inp-field-description', 'cols' => 50, 'rows' => 5]);

            // Required
            $formContent   .= html_writer::label(get_string('field_required', 'local_coursecustomfields') . ': ' . $required, 'inp-field-required')
                            . html_writer::select(
                                $this->yesno,
                                'field_required',
                                $resolveValue('required') ? $this->db_to_sel($resolveValue('required')) : '',
                                ['' => 'choosedots'],
                                ['id' => 'inp-field-required']
                            );

            // Active
            $formContent   .= html_writer::label(get_string('field_active', 'local_coursecustomfields') . ': ' . $required, 'inp-field-active')
                            . html_writer::select(
                                $this->yesno,
                                'field_active',
                                $resolveValue('active') ? $this->db_to_sel($resolveValue('active')) : '',
                                ['' => 'choosedots'],
                                ['id' => 'inp-field-active']
                            );

            // Order
            $formContent   .= html_writer::label(get_string('field_order', 'local_coursecustomfields') . ': ', 'inp-field-order')
                            . html_writer::empty_tag('input', ['type'=>'text', 'name'=>'field_order', 'id' => 'inp-field-order', 'value' => $resolveValue('order') ?: '0']);

            // Buttons
            $formContent   .= html_writer::start_div()
                                . (
                                    $isNew
                                    ? html_writer::empty_tag('input', ['type'=>'submit', 'name'=>'act_addfld', 'value' => get_string('add', 'local_coursecustomfields')])
                                    : html_writer::empty_tag('input', ['type'=>'submit', 'name'=>'act_updtfld', 'value' => get_string('update', 'local_coursecustomfields')])
                                )
                                . html_writer::empty_tag('input', ['type'=>'submit', 'name'=>'act_cancel_add_field', 'value' => get_string('cancel', 'local_coursecustomfields')])
                            . html_writer::end_div();

            // If existing - add field ID
            if (!$isNew)
                $formContent .= html_writer::empty_tag('input', ['type'=>'hidden', 'name'=>'field_id', 'value' => $field_id]);

        $renderOutput .= html_writer::tag('form', $formContent, ['action' => 'index.php', 'method' => 'post', 'id' => 'custom-course-fields-add-form']);
        /**
         * END Form rendering
         */

        echo $renderOutput;
	}
	/*
	 * static checkbox 	( 	$  	name,
	 		$  	value,
	 		$  	checked = true,
	 		$  	label = '',
	 		array $  	attributes = null
	 ) 		[static]
	*/
	function show_input_element($title, $type, $field_id, $value, $description) {
		global $DB;

        $descriptionIcon = '';

        if (!empty($description))
            $descriptionIcon = $this->renderHelpIcon($field_id) . ' ';
		
		switch($type) {
			case self::TEXT_INPUT:
                echo html_writer::label($title . ': ' . $descriptionIcon, 'inp-field-' . $field_id);
				echo html_writer::empty_tag('input', ['type' => 'text', 'name' => 'field_' . $field_id, 'value' => $value, 'id' => 'inp-field-' . $field_id]);
				break;
				
			case self::DROPDOWN_INPUT:
                echo html_writer::label($title . ': ' . $descriptionIcon, 'inp-field-' . $field_id);
				
				// Get the options from the database
				$recs = $DB->get_records('customfield_option', array('customfield_id'=>$field_id));
				
				// Put them in a dropdown list
				$dropdown = array();
				$select = "";
				foreach ($recs as $rec) {
					$dropdown[$rec->id]= $rec->val;
					if(strcmp($rec->id,$value)==0)
						$select=$rec->val;
				}
				echo html_writer::select($dropdown, 'field_'.$field_id, array_search($select, $dropdown), ['' => 'choosedots'], ['id' => 'inp-field-' . $field_id]);
				break;
				
			//case self::CHECKBOX_INPUT:
			//	echo html_writer::checkbox('field_'.$field_id, 'field_'.$field_id, true, $title);				
			//	break;
		}
	}
	
	function showReqIndc() {
		echo '<font color="red">&nbsp;&nbsp;*</font>';
	}
	
	function db_to_sel($value) {
		if($value==DB_YES)
			return SEL_YES;
		else
			return SEL_NO;
	}

	function get_type_text($type) {
		switch($type) {
			case self::TEXT_INPUT:
				return get_string('input_type', 'local_coursecustomfields');
				break;
			case self::DROPDOWN_INPUT:
				return get_string('dropdown_type', 'local_coursecustomfields');
				break;
			//case self::CHECKBOX_INPUT:
			//	return get_string('checkbox_type', 'local_coursecustomfields');
			//	break;			
		}		
		return get_string('unknown_type', 'local_coursecustomfields').'('.$type.')'.TEXT_INPUT;
	}
	
	
	function getDropDownData($field_id) {
		global $DB;
		
		$recs = $DB->get_records('customfield_option', array('customfield_id'=>$field_id));

		// Put them in a dropdown lists
		$dropdown = array();		
		foreach ($recs as $rec) 
			$dropdown[]= $rec->val; 		
		return implode (',' , $dropdown);
	}
	
	function get_record($column, $search, $recordSet) {
		//foreach ($recordSet as $key => $record) {
		foreach ($recordSet as $record) {
			if(strcmp($record->$column, $search)==0) {
			//if ($record->column == $search) {
				return $record;
			}
		}
		return null;
	}
	
	function get_param($form_vars, $param) {
		if ($form_vars) {
			foreach ($form_vars as $key => $value) {
				if(strcmp($key, $param)==0) {
					return $value;
				}
			}
		}
	}

    /**
     * Implementation of user image rendering.
     *
     * @param int $customFieldId
     *
     * @return string HTML fragment
     */
    protected function renderHelpIcon($customFieldId) {
        global $CFG, $OUTPUT;

        // first get the help image icon
        $src = $OUTPUT->pix_url('help');

        $title = get_string('description_title', 'local_coursecustomfields');
        $alt = get_string('helpwiththis');

        $attributes = ['src' => $src, 'alt' => $alt, 'class' => 'iconhelp'];
        $output = html_writer::empty_tag('img', $attributes);

        // now create the link around it - we need https on loginhttps pages
        $url = new moodle_url($CFG->httpswwwroot.'/local/coursecustomfields/icon_popup.php', ['fieldId' => $customFieldId]);

        $attributes = ['href' => $url, 'title' => $title, 'aria-haspopup' => 'true', 'target'=>'_blank'];
        $output = html_writer::tag('a', $output, $attributes);

        // and finally span
        return html_writer::tag('span', $output, ['class' => 'helptooltip']);
    }

    /**
     * Implementation of user image rendering.
     *
     * @param help_icon $helpicon A help icon instance
     * @return string HTML fragment
     */
    //protected function render_help_icon(help_icon $helpicon) {
    //    global $CFG;
    //
    //    // first get the help image icon
    //    $src = $this->pix_url('help');
    //
    //    $title = get_string($helpicon->identifier, $helpicon->component);
    //
    //    if (empty($helpicon->linktext)) {
    //        $alt = get_string('helpprefix2', '', trim($title, ". \t"));
    //    } else {
    //        $alt = get_string('helpwiththis');
    //    }
    //
    //    $attributes = array('src'=>$src, 'alt'=>$alt, 'class'=>'iconhelp');
    //    $output = html_writer::empty_tag('img', $attributes);
    //
    //    // add the link text if given
    //    if (!empty($helpicon->linktext)) {
    //        // the spacing has to be done through CSS
    //        $output .= $helpicon->linktext;
    //    }
    //
    //    // now create the link around it - we need https on loginhttps pages
    //    $url = new moodle_url($CFG->httpswwwroot.'/help.php', array('component' => $helpicon->component, 'identifier' => $helpicon->identifier, 'lang'=>current_language()));
    //
    //    // note: this title is displayed only if JS is disabled, otherwise the link will have the new ajax tooltip
    //    $title = get_string('helpprefix2', '', trim($title, ". \t"));
    //
    //    $attributes = array('href' => $url, 'title' => $title, 'aria-haspopup' => 'true', 'target'=>'_blank');
    //    $output = html_writer::tag('a', $output, $attributes);
    //
    //    // and finally span
    //    return html_writer::tag('span', $output, array('class' => 'helptooltip'));
    //}
}