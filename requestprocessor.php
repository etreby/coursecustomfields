<?php
namespace local_coursecustomfields;

use Exception;
use local\coursecustomfields\MessageHandler;
use stdClass;

class requestprocessor
{
	const TEXT_INPUT = 1;
	const DROPDOWN_INPUT = 2;
	//const CHECKBOX_INPUT = 3;

    /**
     * Error message if error occurred
     *
     * @var object
     */
    public $errorMessage = null;

	function __construct() {
		//echo "In requestprocessor constructor\n";
	}
	
	function reset() {
    	global $ucat_config, $SESSION, $USER, $CFG, $DB, $PAGE, $OUTPUT;
/*		
    	$dbman = $DB->get_manager();
    	if ($dbman->table_exists('customfield')) {
    		echo 'yes';
    	}
    	else {
    		echo 'no';    		
    	}
*/    	
    	//$DB->delete_records('customfield');
    	
		$record = new stdClass();
		//$record-> = 'test';  //'id' // identifier
		$record->name = 'test'; //'name' // name of custom field
		$record->type = 1; //'type' // 1 = text box 2= drop-down etc.
		$record->default_value = ''; //'default_value' // if any default value for custom field
		$record->required = 0; //'required' // required? 0=NO, 1=YES
		$record->domain = 1; //'domain ' // 1=Course custom field, 2=Student custom field  etc.
		$record->active = 1; //'active' // active?  0=NO, 1=YES
		$record->createdate = date ("Y-m-d H:i:s", time()); //'createdate' // DATETIMsE
		
		$lastinsertid = $DB->insert_record('customfield', $record, false);
	}

    /**
     * Validates the course fields
     *
     * @param array $form_vars  - data from form
     * @param array $recs       - data from DB
     *
     * @return boolean
     */
	function validateCourseFields($form_vars, $recs) {
		try {
			foreach ($recs as $rec) {
			    if ($rec->active == DB_NO)
			        continue;

				$fieldId = 'field_' . $rec->id;

				// If the field is required but empty in the parameters
                if($rec->required == DB_YES && empty($form_vars[$fieldId])) {
                    MessageHandler::addErrorMessage(get_string('msg_required_fields', 'local_coursecustomfields'), $fieldId);
                    throw new Exception('');
                }
			}
		
			return true;
		}
		catch (Exception $e) {
		    // Nothing
		}
		
		return false;
	}

    /**
     * Retrieve defined course custom fields
     *
     * @return array|null
     */
	function getDefinedCustomFields()
    {
		global $DB;

		$recs = null;

		try {
			$recs = $DB->get_records('customfield', ['domain' => 1], "field_order ASC");
            return $recs;
		}
		catch (Exception $e) {
		    MessageHandler::addErrorMessage('Cannot retrieve added custom fields!');
		}

		return null;
	}
	
	/**
	 * Add the course values to the database
	 *
	 * @param array $formData
	 * @throws Exception
	 * @return number
	 */
	function addCourseFields($formData, $recs)
	{
		global $DB;

		try {
			if (empty($formData['course_id'])) {
				throw new Exception('Missing course ID');
			}

			$DB->delete_records('customfield_value', ['ref_id' => $formData['course_id']]);

			// For each custom field defined
			foreach ($recs as $rec) {
				if (!$rec->active)
					continue;

				// Get the value
				$value = $formData['field_' . $rec->id];
				if(!empty($value)) {
					$record                 = new stdClass();
					$record->value          = $value;
					$record->customfield_id = $rec->id;
					$record->ref_id         = $formData['course_id'];
					$record->createdate     = date('Y-m-d H:i:s');

					$DB->insert_record('customfield_value', $record, false);
				}
			}

			MessageHandler::addSuccessMessage('Course fields updated!');
			return true;
		}
		catch (Exception $e) {
			MessageHandler::addErrorMessage($e->getMessage());
			return false;
		}
	}

    /**
     * Validates custom course field creation/editing data
     *
     * @param array $form_vars - Data from the form, user input
     *
     * @return bool
     */
	function validateFields($form_vars)
    {
		try {
			if(empty($form_vars['field_name'])) {
				throw new Exception(get_string('msg_namerequired', 'local_coursecustomfields'));
			}
			
			if(empty($form_vars['field_required'])) {
				throw new Exception(get_string('msg_requiredrequired', 'local_coursecustomfields'));
			}
				
			if(empty($form_vars['field_active'])) {
				throw new Exception(get_string('msg_active_required', 'local_coursecustomfields'));
			}
			
			if(!empty($form_vars['field_order']) && !is_numeric($form_vars['field_order'])) {
                throw new Exception(get_string('msg_order_number', 'local_coursecustomfields'));
			}

            if (empty($form_vars['field_type'])) {
                throw new Exception(get_string('msg_type_required', 'local_coursecustomfields'));
            }

            if ($form_vars['field_type'] == self::DROPDOWN_INPUT && empty($form_vars['field_dd_values'])) {
                throw new Exception(get_string('msg_dd_values_required', 'local_coursecustomfields'));
            }
		}
		catch (Exception $e) {
			MessageHandler::addErrorMessage($e->getMessage());
            return false;
		}

        return true;
	}

    /**
     * Adds a new custom course field to the DB
     *
     * @param array $formData
	 *
	 * @return boolean
     */
	function addField($formData)
	{
		global $DB;

		try {
			// Add record
			$record     = $this->transformParamToRecord($formData);
			$fieldId    = $DB->insert_record('customfield', $record, true);

			// Add drop down data
			if ($record->type == self::DROPDOWN_INPUT) {
				$this->addDropDownData($fieldId, $formData);
			}
		}
		catch (Exception $e) {
			MessageHandler::addErrorMessage($e->getMessage());
			return false;
		}
				
        MessageHandler::addSuccessMessage('Custom course field created successfully!');
		return true;
	}

    /**
     * @param $form_vars
     *
     * @return int
     *
     * @return bool|object
     */
	function updateField($form_vars) {
		global $DB;

		$record = $this->transformParamToRecord($form_vars);
		$record->id = $form_vars['field_id'];
			
		try {
			$DB->update_record('customfield', $record, ['id' => $record->id]);

			// Delete all, jic
			///$DB->delete_records('customfield_option', ['customfield_id' => $record->id]);
							
			// Handle drop downs
			if ($form_vars['field_type'] == self::DROPDOWN_INPUT) {
				$this->addDropDownData($record->id, $form_vars);
			}
		}
		catch (Exception $e) {
			MessageHandler::addErrorMessage($e->getMessage());
			return false;
		}

		MessageHandler::addSuccessMessage('Field updated successfully!');
        return $record;
	}

    /**
     * Inserts into DB data for custom form field of type 'drop-down'.
     *
     * @param int   $field_id
     * @param array $formData
     *  [125]=> object(stdClass)#103 (4) { 
     *  	["id"]=> string(3) "125" 
     *  	["customfield_id"]=> string(2) "75" 
     *  	["val"]=> string(3) "YES" 
     *  	["display_order"]=> string(1) "1" }
     */
	function addDropDownData($field_id, $formData)
    {
		global $DB;

		// New items
        $new_items = explode(',', $formData['field_dd_values']);
        //var_dump($new_items);
        
        // Old items
        $recs = $DB->get_records('customfield_option', ['customfield_id' => $field_id]);
        //var_dump($recs);
                
        // Delete any records that are taken away
        $existing_items = array();
        foreach ($recs as $rec) {
        	// If the record not in the new items
        	if($this->in_array_case_insensitive($rec->val, $new_items)==false) {
        		//echo 'Deleteing '.$rec->val;
				$DB->delete_records('customfield_value', array('customfield_id'=>$rec->id));
        		$DB->delete_records('customfield_option', array('id'=>$rec->id));        		
        	} 
        	else {
        		$existing_items[] = $rec->val;
        	}        	 
        }

        $order = 0;
        foreach ($new_items as $new_item) {        	
        	if($this->in_array_case_insensitive($new_item, $existing_items)==false) {
        		//echo PHP_EOL.'Adding '.$new_item;        		
	            $DB->insert_record(
	                'customfield_option',
	                (object) [
	                    'customfield_id' => $field_id,
	                    'val'            => $new_item,
	            		'display_order'  => 1,
	                ]
	            );
        	}
        }
        
        // Get updated db list
        $recs = $DB->get_records('customfield_option', ['customfield_id' => $field_id]);   
        //var_dump($recs);
        $order = 1;
        foreach ($new_items as $new_item) {        	
        	foreach ($recs as $rec) {
        		if(strcasecmp($rec->val, $new_item) == 0) {
        			$updateRec = (object) ['val' => $new_item,
	                    					'customfield_id' => $field_id,
        									'display_order' => $order,
        									'id' => $rec->id
        									];
        			//var_dump($updateRec);
        			$DB->update_record('customfield_option', $updateRec, ['id' => $rec->id]);
        		}  
        	}
        	$order=$order+1;
        }        	
        
	}
	
	function in_array_case_insensitive($needle, $haystack)
	{
		return in_array( strtolower($needle), array_map('strtolower', $haystack) );
	}

    /**
     * Transforms user input to object that can be saved to DB
     *
     * @param array $userFormInput
     *
     * @return stdClass
     */
	function transformParamToRecord(array $userFormInput)
    {
        return (object) [
            'name'          => $userFormInput['field_name'],
            'type'          => $userFormInput['field_type'],
            'default_value' => empty($userFormInput['field_default_value']) ? '' : $userFormInput['field_default_value'],
            'description'   => empty($userFormInput['field_description']) ? '' : $userFormInput['field_description'],
            'required'      => $userFormInput['field_required'] - 1,
            'domain'        => 1,
            'active'        => $userFormInput['field_active'] - 1,
            'createdate'    => date('Y-m-d H:i:s'),
            'field_order'   => (int) $userFormInput['field_order'],
        ];
	}	
	
	function deletefield($id) {
		global $DB;
		$DB->delete_records('customfield_option', array('customfield_id'=>$id));
		$DB->delete_records('customfield_value', array('customfield_id'=>$id));
		$DB->delete_records('customfield', array('id'=>$id));		
	}
	
	function getfield($id) {
		global $DB;
		$recs = null;
		try {
			$rec = $DB->get_record('customfield', array('id' => $id));
		}
		catch (Exception $e) {
		}
		return $rec;		
	}
	
	function get_param($form_vars, $param) {
		if ($form_vars) {
			foreach ($form_vars as $key => $value) {
				if(strcmp($key, $param)==0) {
					return $value;
				}
			}
		}
	}
	
}