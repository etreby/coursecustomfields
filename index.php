<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Analytics
 *
 * This module provides extensive analytics on a platform of choice
 * Currently support Google Analytics and Piwik
 *
 * @package    local_analytics
 * @copyright  David Bezemer <info@davidbezemer.nl>, www.davidbezemer.nl
 * @author     David Bezemer <info@davidbezemer.nl>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
use local\coursecustomfields\MessageHandler;

require('../../config.php');
require_once($CFG->libdir .'/filelib.php');
include_once($CFG->dirroot . '/local/coursecustomfields/renderer.php');
require_once($CFG->dirroot . '/local/coursecustomfields/requestprocessor.php');
require_once($CFG->dirroot . '/local/coursecustomfields/MessageHandler.php');

require_login();
if (!is_siteadmin()) {
	die;
}
define("SEL_YES", 2);
define("SEL_NO", 1);
define("DB_YES", 1);
define("DB_NO", 0);

$behome = $CFG->wwwroot.'/local/coursecustomfields/index.php';

$action = coursecustomfields_param_action();
//echo 'Debug: '.$action;
$processor = new local_coursecustomfields\requestprocessor();
$params = coursecustomfields_get_params();
//var_dump($params);
// Processor commands
$course = "";
$field=null;

switch ($action) {				

	case 'reset':
		$processor->reset();
		break;
		
	case 'delfld':
		$processor->deletefield($_GET['field']);
		break;
		
	case 'updtfld':
		$action = 'updt_fld_error';

		if($processor->validateFields($params)) {
			if ($field = $processor->updateField($params)) {
				$action = 'define';
			}
		}				
		break;
		
	case 'editfld':
		$field = $processor->getfield($_GET['field']);
		break;
		
	case 'addfld':
		$action = 'add_fld_error';

		if($processor->validateFields($params)) {
			if ($processor->addField($params)) {
				$action = 'define';
			}
		}
		break;
		
	case 'set_course_fields':
		$recs = $processor->getDefinedCustomFields();

        if (!MessageHandler::hasErrors()) {
            if ($processor->validateCourseFields($params, $recs)) {
                $processor->addCourseFields($params, $recs);
            }
        }

		$action = 'show_courses_page';
		break;		
}

$PAGE->set_url('/local/coursecustomfields/index.php');
$PAGE->set_cacheable(false);
$PAGE->set_context(context_system::instance());

$title = get_string('pluginname', 'local_coursecustomfields');
$PAGE->set_pagelayout('standard');
$PAGE->set_title($title);
$PAGE->set_heading($title, 3);

/** @var local_coursecustomfields_renderer $output */
$output = $PAGE->get_renderer('local_coursecustomfields');

echo $output->header();
echo $output->heading($title);
$output->behome = $CFG->wwwroot.'/local/coursecustomfields/index.php'; 

// Render commands
switch ($action) {
	case 'updt_fld_error';
	case 'updtfld':	
	case 'editfld';
		$output->render_add_field_form("edit", $params, $field);
		break;
	
	case 'add_fld_error':
	case 'addfld':
	case 'showaddfldform':
		$output->render_add_field_form("add", $params, $field);
		break;
	
	case 'reset':
	case 'delfld':
	case 'cancel_add_field':
	case 'define':
		echo $output->show_define_page($output->behome);
		break;
		
	case 'get_course_fields':	
	case 'show_courses_page':
		$output->showEnterCourseValuesPage($params);
		break;
	
	default:
		echo $output->show();
		break;
}
		
echo $output->footer();

/**
 * Returns the first button action with the given prefix, taken from
 * POST or GET, otherwise returns false.
 * @see /lib/moodlelib.php function optional_param().
 * @param string $prefix 'act_' as in 'action'.
 * @return string The action without the prefix, or false if no action found.
 */
function coursecustomfields_param_action($prefix = 'act_') {
	$action = false;
	if ($_POST) {
		$form_vars = $_POST;
	}
	elseif ($_GET) {
		$form_vars = $_GET;
	}
	else {
		return "";	
	}
	
	if ($form_vars) {
		foreach ($form_vars as $key => $value) {

			if (preg_match("/$prefix(.+)/", $key, $matches)) {
				$action = $matches[1];
				break;
			}
		}		
	}
	if ($action && !preg_match('/^\w+$/', $action)) {
		$action = false;
		print_error('unknowaction');
	}

	return $action;
}

function coursecustomfields_get_params() {
	if ($_POST) {
		return $_POST;
	}
	elseif ($_GET) {
		return $_GET;
	}
	else {
		return null;
	}
}

