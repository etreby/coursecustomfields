<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Analytics
 *
 * This module provides extensive analytics on a platform of choice
 * Currently support Google Analytics and Piwik
 *
 * @package    local_analytics
 * @copyright  David Bezemer <info@davidbezemer.nl>, www.davidbezemer.nl
 * @author     David Bezemer <info@davidbezemer.nl>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/*
$ADMIN->add('roles', new admin_externalpage(
		'toolcapability',
		get_string('pluginname', 'tool_capability'),
		"$CFG->wwwroot/$CFG->admin/tool/capability/index.php",
		'moodle/role:manage'
));
*/

if (is_siteadmin()) {
	$ADMIN->add('localplugins', new admin_externalpage('local_coursecustomfields', get_string('pluginname', 'local_coursecustomfields'), $CFG->wwwroot.'/local/coursecustomfields/index.php'));
}