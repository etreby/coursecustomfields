function ccf_field_type_change(elem)
{
	var dropDownOptions = document.querySelector('.ccf_field_dropdown_values');

	if (elem.value == '2')
		dropDownOptions.style.display = 'block';
	else
		dropDownOptions.style.display = 'none';
}

(
	function()
	{
		// Some cross browser event handling mechanism
		var crossBrowserEventListener = (function() {
			var div;

			// The function we use on standard-compliant browsers
			function standardHookEvent(element, eventName, handler) {
				element.addEventListener(eventName, handler, false);
				return element;
			}

			// The function we use on browsers with the previous Microsoft-specific mechanism
			function oldIEHookEvent(element, eventName, handler) {
				element.attachEvent("on" + eventName, function(e) {
					e = e || window.event;
					e.preventDefault = oldIEPreventDefault;
					e.stopPropagation = oldIEStopPropagation;
					handler.call(element, e);
				});
				return element;
			}

			// Polyfill for preventDefault on old IE
			function oldIEPreventDefault() {
				this.returnValue = false;
			}

			// Polyfill for stopPropagation on old IE
			function oldIEStopPropagation() {
				this.cancelBubble = true;
			}

			// Return the appropriate function; we don't rely on document.body
			// here just in case someone wants to use this within the head
			div = document.createElement('div');
			if (div.addEventListener) {
				div = undefined;
				return standardHookEvent;
			}
			if (div.attachEvent) {
				div = undefined;
				return oldIEHookEvent;
			}
			throw "Neither modern event mechanism (addEventListener nor attachEvent) is supported by this browser.";
		})();

		crossBrowserEventListener(
			document.getElementById('ccf-filter-courses-checkbox'),
			'change',
			function(e)
			{
				var
					target = e.target || e.srcElement,
					original = document.getElementById('inp-course-id'),
					dummy = document.getElementById('inp-course-id-dummy');

				if (target.checked) {
					var existing = original.querySelectorAll('[new]');

					for (var i = 0; i < existing.length; i++) {
						dummy.appendChild(existing[i]);
					}

					original.selectedIndex = 0;
				}
				else {
					while(dummy.childNodes.length > 0) {
						original.appendChild(dummy.childNodes[0]);
					}

					original.selectedIndex = 0;
				}
			}
		);
	}
)();