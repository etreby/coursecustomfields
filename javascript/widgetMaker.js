String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function generate() {
    var Fields = document.getElementById("GenerateWidgetBtn").getAttribute("data-fields");
    Fields = Fields.toString().replaceAll("'",'"');
    Fields = JSON.parse(Fields);
    console.info(Fields);
    var FieldPrifix = 'inp-field-';

    function CheckFieldUndefined(FieldPrifix,field,fieldName) {
        if(field !== 'undefined'){
            var x =  document.getElementById(FieldPrifix + field);
            if(x !== null){
                return x.value;
            }else{
                console.warn(FieldPrifix + field + 'Not Exist !');
                return '';
            }
        }else{
            console.warn(fieldName+ ' :' + FieldPrifix + field);
            return '';
        }
    }

    function CheckFieldSelectValueUndefined(FieldPrifix,field,fieldName) {
        if(field !== 'undefined'){
            var x = document.getElementById(FieldPrifix + field);
            if(x !== null) {
                return x.options[x.selectedIndex].value;
            }else{
                console.warn(FieldPrifix + field + 'Not Exist !');
                return '';
            }
        }else{
            console.warn(fieldName+ ' :' + FieldPrifix + field);
            return '';
        }
    }

    function CheckFieldSelectTextUndefined(FieldPrifix,field,fieldName) {
        if(field !== 'undefined'){
            var x = document.getElementById(FieldPrifix + field);
            if(x !== null) {
                return x.options[x.selectedIndex].text;
            }else{
                console.warn(FieldPrifix + field + 'Not Exist !');
                return '';
            }
        }else{
            console.warn(fieldName+ ' :' + FieldPrifix + field);
            return '';
        }
    }


    /*
     var e = document.getElementById("durtype"),
     durationtype = e.options[e.selectedIndex].value,
     */
    var durationtype = '',
        difficulty = CheckFieldUndefined(FieldPrifix,Fields.SKILL_LEVEL,'SKILL_LEVEL'),
        duration = CheckFieldUndefined(FieldPrifix ,Fields.DURATION,'DURATION'),
        location = CheckFieldSelectTextUndefined(FieldPrifix,Fields.DELIVERY_TYPE_DETAILED,'DELIVERY_TYPE_DETAILED'),
        description = CheckFieldUndefined(FieldPrifix,Fields.SHORT_SUMMARY,'SHORT_SUMMARY'),
        launch = CheckFieldUndefined(FieldPrifix,Fields.SOURCE_URL,'SOURCE_URL'),
        image = CheckFieldUndefined(FieldPrifix,Fields.IMAGE_URL,'SOURCE_URL');


    if(image.length > 0){
        var desClass = 'ibm-col-6-2';
    }else{
        var desClass = 'ibm-col-6-3';
    }
    /*
    var e = document.getElementById("coursetype");
    if(e){
        var coursetype = e.options[e.selectedIndex].value;
    }
    var typesIcons = ['','Play','Article','Blog','Play','Play','Play'];
    */
    var data = "";


    data += "<div class='course-icons-bottom'>"
    if(difficulty.length > 0 && difficulty != 'None'){
        data += "<span class='course-info'><i class='CustomeIcomoon-Level'></i>"+difficulty+"</span>";
    }
    if(duration.length > 0){
        if(duration >= 60){
            duration = Math.floor(duration/60);
            durationtype = 'Hours';
        }else{
            durationtype = 'Minutes';
        }
        data += "<span class='course-info'><i class='CustomeIcomoon-Duration'></i>"+duration+ " " + durationtype +"</span>";
    }
    /*
    if(typesIcons[e.selectedIndex] != ''){
        data += "<span class='course-info'><i class='CustomeIcomoon-"+ typesIcons[e.selectedIndex] +"'></i>"+coursetype.charAt(0).toUpperCase() + coursetype.slice(1) +"</span>";
    }
    */
    if(location.length > 0 && location != 'None'){
        data += "<span class='course-info'><i class='CustomeIcomoon-Location'></i>"+location+"</span>";
    }
    data += "</div>";

    data += "<div class='ibm-columns' style='padding-left:0px!important;margin-left:0px!important;'>";
    data +=        "<div class='"+desClass+"' stlye='margin-left: 0px!important;'>";
    data +=        "<p>"+ description + "</p>";
    data +=        "</div>";
    if(image.length > 0){
        data +=        "<div class='ibm-col-6-1'>";
        data +=            "<img class='ibm-right' style='width:100px;height:100px;' src='" + image + "'>";
        data +=        "</div>";
    }
    data += "</div>";

    var OutputNoBtn = data;
    if(launch.length > 0){
        data += "<div style='margin-top:25px;margin-bottom:5px;'><a id='launchLink' class='ibm-btn-pri ibm-btn-blue-50' href='"+ launch + "'>Launch</a></div>";
    }
    document.getElementById("output").innerHTML = data;
    document.getElementById("outputnobtn").innerHTML = OutputNoBtn;
    document.getElementById("preview").innerHTML = data;
}
generate();
//document.getElementById("GenerateWidgetBtn").addEventListener("click", generate());
function saveCourseDescription(course_id,pleaseWaitTxt,SavedTxt,failedTxt) {
    generate();
    document.getElementById("SaveWidgetStatus").innerHTML= pleaseWaitTxt;
    var http = new XMLHttpRequest();
    var url = "CourseDescription_ajax.php";
    var params = "course_id=" + course_id+"&NewSummary="+ encodeURIComponent(JSON.stringify({'course_id': course_id ,'data' : document.getElementById("output").innerHTML, 'datanobtn' : document.getElementById("outputnobtn").innerHTML}));
    http.open("POST", url, true);
    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.send(params);
    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            var response = JSON.parse(http.responseText);
            console.info(response);
            if(response.text == 'success'){
                document.getElementById("SaveWidgetStatus").innerHTML= SavedTxt;
            }else{
                document.getElementById("SaveWidgetStatus").innerHTML= failedTxt;
            }
        }
    }

}